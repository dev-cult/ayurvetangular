import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';


import { DataService } from '../../../services/data.service';
import { User } from '../../../models/user.model';
@Component({
  selector: 'app-dlregistration',
  templateUrl: './dlregistration.component.html',
  styleUrls: ['./dlregistration.component.css']
})
export class DlregistrationComponent implements OnInit {
	@BlockUI() blockUI: NgBlockUI;
    @ViewChild('auto') auto;
    registerForm: FormGroup;
    submitted = false;
 
    public errors: any;
    public employeedata: any;
    public userForm: any;
    public showentryform: boolean;
  constructor(
  	private dataService: DataService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
    ) { 
  	 this.blockUI.start('Loading...'); // Start blocking
  	 this.errors = [];
  	 this.employeedata = {};
     this.showentryform = false;
  	 // this.employeedata.empGender = 'Male';
  }

  ngOnInit() {
    this.showentryform = true;

     this.registerForm = this.formBuilder.group({
        companyName: ['', Validators.required],
        companyWebsite: ['', Validators.required],
        empCount: ['', Validators.required],
        companyAddress: ['', Validators.required],
        empFirstName: ['', Validators.required],
        empLastName: ['', Validators.required],
        empDesignation: ['', Validators.required],
        empContactNo: ['', Validators.required],
        empEmail: ['', [Validators.required, Validators.email]]
    });
  }
  
  formSubmit() {
  		this.submitted = true;
 		if (this.registerForm.invalid) {
            return;
        }
        var empdataObj = {
	        companyName: this.employeedata.companyName,
	        companyWebsite: this.employeedata.companyWebsite,
	        empCount: this.employeedata.empCount,
	        companyAddress: this.employeedata.companyAddress,

	        empFirstName: this.employeedata.empFirstName,
	        empLastName: this.employeedata.empLastName,
	        empDesignation: this.employeedata.empDesignation,
	        empContactNo: this.employeedata.empContactNo,
	        empEmail: this.employeedata.empEmail
	    }
 		console.log(empdataObj);
   		//alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
        this.blockUI.start('Submitting...'); // Start blocking
			this.dataService.dlregistration(empdataObj).subscribe(
                data => {
                	console.log(data);
                    if(data.code === 200) {
                        if(data.message === 'Success') { 
                          console.log('done');
                          Swal.fire('Addedd!','Details added successfully!', 'success' );
                          this.showentryform = true;
                        	// document.getElementById("closemodal").click();
                         }
                        else if(data.message === 'Failed to add data, try again later.') { console.log('here'); }                    
                    }
                    else {
                        console.log('error');
                    }
                    this.blockUI.stop();
                },
                err => {
                console.log("inside Registration error", JSON.stringify(err.message));
                  this.blockUI.stop();
                }
            );

    }

}

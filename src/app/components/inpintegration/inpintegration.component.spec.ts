import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InpintegrationComponent } from './inpintegration.component';

describe('InpintegrationComponent', () => {
  let component: InpintegrationComponent;
  let fixture: ComponentFixture<InpintegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InpintegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InpintegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit,Input } from '@angular/core';
import { Project } from '../../models/project.model';

declare var ldBar: any;
@Component({
  selector: 'app-compsentiment',
  templateUrl: './compsentiment.component.html',
  styleUrls: ['./compsentiment.component.css']
})
export class CompsentimentComponent implements OnInit {
 @Input() project: Project;
  public rcs: any;
  public topNetworkLeaderSrc: any;
  public topInfluencerSrc: any;
  public majorCliquesSrc: any;
  public topLocalLeaderSrc: any;

  constructor() { 
     this.rcs = [
      {'code': 'rcwl1', 'prop': 'rc_work_life_balance', 'label': 'Social Dynamics', 'icon': 'user-friends', 'pop_desc':'Measures the health of individual interactions and behaviors of the group at the Bank.'},
      {'code': 'rcoe1', 'prop': 'rc_org_experience', 'label': 'Employee Importance', 'icon': 'chart-line', 'pop_desc':'A measure of how important every employee feels in the organization, team and the role they are currently in.'},
      {'code': 'rcsb1', 'prop': 'rc_salary_benefits', 'label': 'Information Flow', 'icon': 'chart-area', 'pop_desc':'Measures the health of information flow across the network, among cliques and influencers in the bank.'},
      {'code': 'rcjc1', 'prop': 'rc_job_security', 'label': 'Community Structure', 'icon': 'circle-notch', 'pop_desc':'A measure of how the cliques, influencers, and the overall network is structured within the organization.'}, 
    ] 

    this.topNetworkLeaderSrc = "";
    this.topInfluencerSrc = "";
    this.majorCliquesSrc = "";
    this.topLocalLeaderSrc = "";
  }
  
  showKnob(el, prop) {
    var bar = new ldBar('#' + el);
    bar.set( (this.project.pskcs[prop] * 20));
    //$('.' + el).data('percent', (this.project.lsp[prop] * 10));
    //$('.' + el).loading();
  }

  hideKnob(el, prop) {
    var bar = new ldBar('#' + el);
    bar.set(0);
    //$('.' + el).data('percent', 0);
    //$('.' + el).loading();
  }

   initStuff(){
    let self = this;
    
    self.topNetworkLeaderSrc = "";
    self.topInfluencerSrc = "";
    self.majorCliquesSrc = "";
    self.topLocalLeaderSrc = "";

    setTimeout(function() { 
        for(let rc of self.rcs) {
          self.hideKnob(rc.code, rc.prop);
          self.showKnob(rc.code, rc.prop);
        }
        self.topNetworkLeaderSrc = "load";
        self.topInfluencerSrc = "load";
        self.majorCliquesSrc = "load";
        self.topLocalLeaderSrc = "load";
    }, 1000);
  }
  ngOnInit() {
     console.log(this.project);
     console.log(this.project.project_variation_type);
    this.project.pskcs = {
      "executiveSummary": "The Organizational Network Analysis was conducted and hearing 65% of your organization. Through the detailed report below we understand how the organization network operates to transfer information, handle friction and collaborate across functions. It helps us draw direct inferences to,\n\
                        <br>1. Predict attrition<br>2. Build future leaders <br>3. Identify the right organizational structure",
      "opinion_index": 1.25,
      "review": "1170",
      "interview":"693",
      "ceo_approval":"84",
      "friend_recommend":"62",
      "rc_work_life_balance": 3.7,
      "rc_org_experience": 4.2,
      "rc_salary_benefits": 2.4,
      "rc_job_security": 4.3,
      "rc_management": 2.4
    };


    
  }


}

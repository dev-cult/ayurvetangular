import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BnsintegrationComponent } from './bnsintegration.component';

describe('BnsintegrationComponent', () => {
  let component: BnsintegrationComponent;
  let fixture: ComponentFixture<BnsintegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BnsintegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BnsintegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

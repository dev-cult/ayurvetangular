import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EumComponent } from './eum.component';

describe('EumComponent', () => {
  let component: EumComponent;
  let fixture: ComponentFixture<EumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit,Input } from '@angular/core';
import { Project } from '../../models/project.model';

declare var ldBar: any;

@Component({
	selector: 'app-compsentimentintegration',
	templateUrl: './compsentimentintegration.component.html',
	styleUrls: ['./compsentimentintegration.component.css']
})
export class CompsentimentintegrationComponent implements OnInit {

	@Input() project: Project;
	public rcs: any;
  	public currentView: any;
  	public selectednetwork: any;

	constructor() {
		this.rcs = [
		{'code': 'rcwl1', 'prop': 'rc_work_life_balance', 'label': 'Social Dynamics', 'icon': 'user-friends', 'pop_desc':'Measures the health of individual interactions and behaviors of the group at the Bank.'},
		{'code': 'rcoe1', 'prop': 'rc_org_experience', 'label': 'Employee Importance', 'icon': 'chart-line', 'pop_desc':'A measure of how important every employee feels in the organization, team and the role they are currently in.'},
		{'code': 'rcsb1', 'prop': 'rc_salary_benefits', 'label': 'Information Flow', 'icon': 'chart-area', 'pop_desc':'Measures the health of information flow across the network, among cliques and influencers in the bank.'},
		{'code': 'rcjc1', 'prop': 'rc_job_security', 'label': 'Community Structure', 'icon': 'circle-notch', 'pop_desc':'A measure of how the cliques, influencers, and the overall network is structured within the organization.'},
		
		]
    	this.currentView = 'bnsEnt1';
	}
	showKnob(el, prop) {
		var bar = new ldBar('#' + el);
		bar.set( (this.project.pskcs[prop] * 20));
	}

	hideKnob(el, prop) {
		var bar = new ldBar('#' + el);
		bar.set(0);
	}
	initStuff(){
		console.log("company sentiment integration");
		let self = this;
		setTimeout(function() { 
			for(let rc of self.rcs) {
				self.hideKnob(rc.code, rc.prop);
				self.showKnob(rc.code, rc.prop);
			}

		}, 1000);
	}
	ngOnInit() {
		this.project.pskcs = {
			"executiveSummary": "The Organizational Network Analysis covered 64% of the employees. Through the detailed report below we understand how the bank network operates to transfer information, handle friction and collaborate across functions. It helps us draw direct inferences to,\n\
			<br>1. Group Dynamics<br>2. Identify Influencers  <br>3. Inferences to the adoption of effective communication channels",
			"opinion_index": 1.25,
			"review": "1170",
			"interview":"693",
			"ceo_approval":"84",
			"friend_recommend":"62",
			"rc_work_life_balance": 3.7,
			"rc_org_experience": 4.2,
			"rc_salary_benefits": 2.4,
			"rc_job_security": 4.3,
			"rc_management": 2.4
		};

	}
	switchView(view) {
		this.currentView = view;
	}

}

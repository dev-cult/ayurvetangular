import { Component, OnInit, Input } from '@angular/core';
import { Project } from '../../models/project.model';
import { from } from 'rxjs';
import { pluck, groupBy } from 'rxjs/operators';
import { KeyValuePipe } from '@angular/common';

declare var ldBar: any;

@Component({
	selector: 'app-valueassessmentintegrationentitya',
	templateUrl: './valueassessmentintegrationentitya.component.html',
	styleUrls: ['./valueassessmentintegrationentitya.component.css']
})
export class ValueassessmentintegrationentityaComponent implements OnInit {

	@Input() project: Project;
	public rcs: any;
	public ivop: any;
	public ivls: any;
	public ivls_op1:any;
	public ivls_op2: any;
	public ivls_op3:any;
	public ivoptions :any;
	public currentView: any;
	public cvls: any;
	public branchno: any;
	public uniqueresponse: any;
	public responsedata_per: any;
	
    public countUpView: any;
	public noofparticipant: any;
	public noofresponse: any;
	public noofdepartments: any;
	public opinianindex: any;

 
	constructor() { 
		console.log(this.project);
		this.rcs = [
		{'code': 'rcwl', 'prop': 'rc_work_life_balance', 'label': 'Personal Values', 'icon': 'balance-scale', 'opvalue': 'op1'},
		{'code': 'rcoe', 'prop': 'rc_org_experience', 'label': 'Current Organizational Values', 'icon': 'sitemap', 'opvalue': 'op2'},
		{'code': 'rcsb', 'prop': 'rc_salary_benefits', 'label': 'Desired Organizational Values', 'icon': 'wallet', 'opvalue': 'op3'}
		];
		this.ivls_op1 = [
		{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [{'label': 'Ethics: 34%', 'state': 'PV'},] },
		{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Leadership: 40%', 'state': 'PV'},] },
		{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Enthusiasm/Positive Attitude: 42%', 'state':'PV'}] },
		{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Teamwork: 66%', 'state':'PV'},{'label': 'Continuous Learning: 39%', 'state':'PV'},{'label': 'Balance (Work/life): 32%', 'state':'PV'}] },
		{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Achievement: 39%', 'state': 'LV'},{'label': 'Professional Growth: 35%', 'state': 'PV'}] },
		{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Family: 36%', 'state':'PV'}] },
		{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [{'label': 'Self Discipline: 62%', 'state':'PV'}] }
		];
		this.ivls_op2 = [
		{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [{'label': 'Ethics: 40%', 'state':'PV'},{'label': 'Vision: 29%', 'state':'PV'}] },
		{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Leadership: 27%', 'state':'PV'}] },
		{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Trust: 33%', 'state':'PV'},{'label': 'Integrity: 31%', 'state':'PV'}] },
		{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Balance (Work/life): 46%', 'state': 'PV'},{'label': 'Teamwork: 65%', 'state':'PV'}] },
		{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Professional Growth: 28%', 'state':'PV'}] },
		{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Recognition: 26%', 'state': 'LV'}] },
		{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [{'label': 'Job Security: 31%', 'state': 'LV'}] }
		];
		this.ivls_op3 = [
		{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [{'label': 'Vision: 45%', 'state': 'PV'},{'label': 'Ethics: 31%', 'state': 'PV'}] },
		{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Leadership: 45%', 'state':'PV'}] },
		{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Creativity: 33%', 'state':'PV'}] },
		{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Teamwork: 49%', 'state':'PV'}] },
		{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Professional Growth: 39%', 'state':'PV'},{'label': 'Reward: 32%', 'state':'LV'},{'label': 'Excellence: 29%', 'state':'PV'}] },
		{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Recognition: 40%', 'state': 'LV'}] },
		{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [{'label': 'Financial stability: 34%', 'state': 'LV'}] }
		];
		this.ivop = 'op1';
		this.ivls = this.ivls_op1;
		this.countUpView = false;
	}

	showKnob(el, prop) {
		var bar = new ldBar('#' + el);
		bar.set( (this.project.cvac[prop] * 10));
	}

	hideKnob(el, prop) {
		var bar = new ldBar('#' + el);
		bar.set(0);
	} 
	ivopChange(opval){
		this.ivop = opval;
		if(this.ivls !== undefined){
			if(opval == 'op1')this.ivls = this.ivls_op1;

			if(opval == 'op2')this.ivls = this.ivls_op2;

			if(opval == 'op3')this.ivls = this.ivls_op3;
		}  
	}
 resetCountUp() {
    this.noofparticipant = 0;
	this.noofresponse = 0;
	this.noofdepartments = 0;
    this.countUpView = false;
    this.opinianindex = 0;
  }

  setCountUp() {
    this.noofparticipant = 95;
	this.noofresponse = 89;
	this.noofdepartments = 15;
    this.opinianindex = 22;
    this.countUpView = true;
  }

  setCountUp1() {
    this.noofparticipant = 9;
	this.noofresponse = 8;
	this.noofdepartments = 1;
    this.opinianindex = 2;
    this.countUpView = true;
  }

  	initStuff() {
		let self = this;
		self.resetCountUp();
		setTimeout(function() { 
			self.setCountUp();
		}, 100);
	}

	ngOnInit() {
		this.project.cvac = {
			"executiveSummary": "This report gives us an overview of what is important to the customers, their current experiences and what they view to be important for the future of the organization.",
			"opinion_index": 3.7,
			"review": "1170",
			"interview":"693",
			"ceo_approval":"84",
			"friend_recommend":"62",
			"rc_work_life_balance": 3.7,
			"rc_org_experience": 4.2,
			"rc_salary_benefits": 2.4,
			"rc_job_security": 5.3,
			"rc_management": 2.4
		};
		
		
		this.onclick();
	}
	onclick() {
 
		console.log(this.project);
		let self = this;
	  self.resetCountUp();
	  this.currentView = 'bnsEnt1';
	
		self.rcs = [
			{'code': 'rcwl', 'prop': 'rc_work_life_balance', 'label': 'Personal Values', 'icon': 'balance-scale', 'opvalue': 'op1'},
			{'code': 'rcoe', 'prop': 'rc_org_experience', 'label': 'Current Organizational Values', 'icon': 'sitemap', 'opvalue': 'op2'},
			{'code': 'rcsb', 'prop': 'rc_salary_benefits', 'label': 'Desired Organizational Values', 'icon': 'wallet', 'opvalue': 'op3'}
			];
			this.ivls_op1 = [
			{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [{'label': 'Ethics: 34%', 'state': 'PV'},] },
			{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Leadership: 40%', 'state': 'PV'},] },
			{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Enthusiasm/Positive Attitude: 42%', 'state':'PV'}] },
			{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Teamwork: 66%', 'state':'PV'},{'label': 'Continuous Learning: 39%', 'state':'PV'},{'label': 'Balance (Work/life): 32%', 'state':'PV'}] },
			{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Achievement: 39%', 'state': 'LV'},{'label': 'Professional Growth: 35%', 'state': 'PV'}] },
			{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Family: 36%', 'state':'PV'}] },
			{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [{'label': 'Self Discipline: 62%', 'state':'PV'}] }
			];
			this.ivls_op2 = [
			{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [{'label': 'Ethics: 40%', 'state':'PV'},{'label': 'Vision: 29%', 'state':'PV'}] },
			{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Leadership: 27%', 'state':'PV'}] },
			{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Trust: 33%', 'state':'PV'},{'label': 'Integrity: 31%', 'state':'PV'}] },
			{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Balance (Work/life): 46%', 'state': 'PV'},{'label': 'Teamwork: 65%', 'state':'PV'}] },
			{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Professional Growth: 28%', 'state':'PV'}] },
			{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Recognition: 26%', 'state': 'LV'}] },
			{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [{'label': 'Job Security: 31%', 'state': 'LV'}] }
			];
			this.ivls_op3 = [
			{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [{'label': 'Vision: 45%', 'state': 'PV'},{'label': 'Ethics: 31%', 'state': 'PV'}] },
			{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Leadership: 45%', 'state':'PV'}] },
			{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Creativity: 33%', 'state':'PV'}] },
			{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Teamwork: 49%', 'state':'PV'}] },
			{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Professional Growth: 39%', 'state':'PV'},{'label': 'Reward: 32%', 'state':'LV'},{'label': 'Excellence: 29%', 'state':'PV'}] },
			{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Recognition: 40%', 'state': 'LV'}] },
			{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [{'label': 'Financial stability: 34%', 'state': 'LV'}] }
			];
			self.ivopChange('op1');
  
	  setTimeout(function() { 
		for(let rc of self.rcs) {
		   self.hideKnob(rc.code, rc.prop);
		  self.showKnob(rc.code, rc.prop);
		}
		self.setCountUp();
  
	}, 100);
  
	  this.project.pskc = {
		"executiveSummary": "People Speak is a comprehensive report telling your organizations's story with actionable insights taking into account factors that offer more value than internal employee engagement surveys.",
		"opinion_index": 3.7,
		"pro_index": "Working Condition",
		"pro_index2": "Growth Opportunities, Benefits and Allowances",
		"pro_index3": "Less pressure in sales",
		"con_index": "Poor work-life balance",
		"con_index2": "Short-Staffed",
		"con_index3": "Worst HR policies and office politics",
		"review": "1194",
		"interview":"45",
		"ceo_approval":"52",
		"friend_recommend":"75",
		"rc_work_life_balance1": 3.2,
		"rc_org_experience1": 3.5,
		"rc_salary_benefits1": 3.5,
		"rc_job_security": 3.8,
		"rc_management": 3.2
	  };
	}
	public projObj: any;
	public currentDash: any;
	public kpi: any;
	
	bnsEnt2() {
		
		console.log(this.project);
		let self = this;
		self.resetCountUp();
		self.rcs = [
			{'code': 'rcwl', 'prop': 'rc_work_life_balance', 'label': 'Personal Values', 'icon': 'balance-scale', 'opvalue': 'op1'},
			{'code': 'rcoe', 'prop': 'rc_org_experience', 'label': 'Current Organizational Values', 'icon': 'sitemap', 'opvalue': 'op2'},
			{'code': 'rcsb', 'prop': 'rc_salary_benefits', 'label': 'Desired Organizational Values', 'icon': 'wallet', 'opvalue': 'op3'}
			];
			this.ivls_op1 = [
			{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [] },
			{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Leadership: 40%', 'state': 'PV'},] },
			{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Enthusiasm/Positive Attitude: 42%', 'state':'PV'}] },
			{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Teamwork: 66%', 'state':'PV'},{'label': 'Continuous Learning: 39%', 'state':'PV'},{'label': 'Balance (Work/life): 32%', 'state':'PV'}] },
			{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Achievement: 39%', 'state': 'LV'},{'label': 'Professional Growth: 35%', 'state': 'PV'}] },
			{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Family: 36%', 'state':'PV'}] },
			{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [{'label': 'Self Discipline: 62%', 'state':'PV'}] }
			];
			this.ivls_op2 = [
			{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [{'label': 'Ethics: 40%', 'state':'PV'},{'label': 'Vision: 29%', 'state':'PV'}] },
			{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Leadership: 27%', 'state':'PV'}] },
			{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Trust: 33%', 'state':'PV'},{'label': 'Integrity: 31%', 'state':'PV'}] },
			{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Balance (Work/life): 46%', 'state': 'PV'},{'label': 'Teamwork: 65%', 'state':'PV'}] },
			{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Professional Growth: 28%', 'state':'PV'}] },
			{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Recognition: 26%', 'state': 'LV'}] },
			{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [{'label': 'Job Security: 31%', 'state': 'LV'}] }
			];
			this.ivls_op3 = [
			{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [{'label': 'Vision: 45%', 'state': 'PV'},{'label': 'Ethics: 31%', 'state': 'PV'}] },
			{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Leadership: 45%', 'state':'PV'}] },
			{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Creativity: 33%', 'state':'PV'}] },
			{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Teamwork: 49%', 'state':'PV'}] },
			{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Professional Growth: 39%', 'state':'PV'},{'label': 'Reward: 32%', 'state':'LV'},{'label': 'Excellence: 29%', 'state':'PV'}] },
			{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Recognition: 40%', 'state': 'LV'}] },
			{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [{'label': 'Financial stability: 34%', 'state': 'LV'}] }
			];
			self.ivopChange('op1');
			this.noofparticipant = 9;
	self.noofresponse = 8;
	self.noofdepartments = 1;
    self.opinianindex = 2;
    self.countUpView = true;
	
	  this.currentView = 'bnsEnt2';
	  this.currentDash = 'eva';
		this.projObj.currentTab = 'eva';
		this.kpi.kpi_code = 'eva';
	  setTimeout(function() { 
		for(let rc of self.rcs) {
		   self.hideKnob(rc.code, rc.prop);
		  self.showKnob(rc.code, rc.prop);
		}
		
	}, 100
	);
		
	  this.project.pskc = {
		"executiveSummary": "People Speak is a comprehensive report telling your organizations's story with actionable insights taking into account factors that offer more value than internal employee engagement surveys.",
		"opinion_index": 3.7,      
		"pro_index": "Work Environment",
		"pro_index2": "Work - Life Balance",
		"pro_index3": "Facilities and Leave Policies",
		"con_index": "Salaries and Promotions",
		"con_index2": "Interior Job Locations",
		"con_index3": "Slow decision making",
		"review": "9356",
		"interview":"-",
		"ceo_approval":"80",
		"friend_recommend":"69",
		"rc_work_life_balance1": 3.8,
		"rc_org_experience1": 3.5,
		"rc_salary_benefits1": 3.4,
		"rc_job_security": 3.4,
		"rc_management": 3.2
	  };
	}

}

import { Component, OnInit, Input } from '@angular/core';
 
import { Project } from '../../models/project.model';

@Component({
  selector: 'app-adaptqintegration',
  templateUrl: './adaptqintegration.component.html',
  styleUrls: ['./adaptqintegration.component.css']
})
export class AdaptqintegrationComponent implements OnInit {

  @Input() project: Project;
	adaptqdash: any;
	public poscore: any;
	public mdindex: any;
	public adapq: any;
  public currentView: any;

  public countUpView: boolean;
  public veryhappy_score: any;
  public happy_score: any;
  public justok_score: any;
  public sad_score: any;
  public verysad_score: any;

  constructor() {
  	this.poscore = [];
  	 this.poscore = {'0':'0','1':'0'}
  	 this.mdindex = [
      {'color':'#188a34', 'label': 'Very Happy', 'icon': 'smile-beam', 'modindex': 25.8},
      {'color':'#90b240', 'label': 'Happy', 'icon': 'smile', 'modindex': 48.4},
      {'color':'#ffc02e', 'label': 'Just Ok', 'icon': 'meh', 'modindex': 16.1},
      {'color':'#ff622b', 'label': 'Sad', 'icon': 'frown-open', 'modindex': 6.5},
      {'color':'#d94334', 'label': 'Very Sad', 'icon': 'frown', 'modindex': 3.2}
    ];
    this.adapq = {
    	"overall_adq_score": "48"
    }
  	this.adaptqdash = {
  		"executiveSummary": "<p> Adaptability quotient is an individuals ability to change. The view here give multiple perspectives on how to make the whole bank adaptive to change. The range for our individual adaptability is between 0 to 25 and the overall organization adaptability is calculated through the weighted average method.</p>",
  		"receptiveness": "Receptiveness to change",
  		 "opinion_index": 2.4

  	}
    this.currentView = 'bnsEnt1';
    this.countUpView = false;
   }


  resetCountUp() {
    this.veryhappy_score = 0;
    this.happy_score = 0;
    this.justok_score = 0;
    this.sad_score = 0;
    this.verysad_score = 0;
    this.countUpView = false;
  }

  setCountUp() {
   this.veryhappy_score = 25.8;
    this.happy_score = 48.5;
    this.justok_score = 16.1;
    this.sad_score = 6.5;
    this.verysad_score = 3.2;
    this.countUpView = true;
  }
  initStuff(){  
    let self = this;
    self.resetCountUp();
    setTimeout(function() { 
       self.setCountUp();
    }, 100);
  }

  ngOnInit() {
  }

  switchView(view) {
    this.currentView = view;
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadershippulseintegrationComponent } from './leadershippulseintegration.component';

describe('LeadershippulseintegrationComponent', () => {
  let component: LeadershippulseintegrationComponent;
  let fixture: ComponentFixture<LeadershippulseintegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadershippulseintegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadershippulseintegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlemployeeComponent } from './dlemployee.component';

describe('DlemployeeComponent', () => {
  let component: DlemployeeComponent;
  let fixture: ComponentFixture<DlemployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlemployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlprojectsComponent } from './dlprojects.component';

describe('DlprojectsComponent', () => {
  let component: DlprojectsComponent;
  let fixture: ComponentFixture<DlprojectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlprojectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlprojectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

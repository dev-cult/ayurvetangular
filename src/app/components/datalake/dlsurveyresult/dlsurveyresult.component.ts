import { Component, OnInit, Input } from '@angular/core';

import { DataService } from '../../../services/data.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute } from '@angular/router';
import { Project } from '../../../models/project.model';
declare var require: any;
var Highcharts = require('highcharts/highcharts.js');

@Component({
	selector: 'app-dlsurveyresult',
	templateUrl: './dlsurveyresult.component.html',
	styleUrls: ['./dlsurveyresult.component.css']
})
export class DlsurveyresultComponent implements OnInit {
	@BlockUI() blockUI: NgBlockUI;
	public surveyCode: any;
	public linkExpired: any;
	public userDetails: any;
	public userResult: any;
	public chartData: any;
	constructor(
		private dataService: DataService,
		private route: ActivatedRoute,
		) { 
		this.surveyCode = this.route.snapshot.params.surveyCode;
		this.linkExpired = false;
		this.userDetails = '';
		this.userResult = '';
		this.chartData = [];
	}

	ngOnInit() {
		if(this.surveyCode !== undefined && this.surveyCode !== '') {
			let survey_code = {
				'survey_code': this.surveyCode
			};

			this.dataService.getComAssSurveyResult(survey_code).subscribe(
				data => {
					console.log(data);
					this.userDetails = data.user_details;
					this.userResult = data.user_result;
					let secA = this.userResult.section_per.secA;
					let secB = this.userResult.section_per.secB;
					let secC = this.userResult.section_per.secC;
					let secD = this.userResult.section_per.secD;
					if(secB != 0) {
						let qB = [-(secB),0];
						this.chartData.push(qB);
					}
					if(secA != 0) {
						let qA = [0,secA];
						this.chartData.push(qA);
					}
					if(secD != 0) {
						let qD = [secD,0];
						this.chartData.push(qD);
					}
					if(secC != 0) {
						let qC = [0,-(secC)];
						this.chartData.push(qC);
					}
					if(secB != 0) {
						let qB = [-(secB),0];
						this.chartData.push(qB);
					}
					// this.chartData = [
					// 	[-(secB), 0],
					// 	[0, secA],
					// 	[secD, 0],
					// 	[0, -(secC)],
					// 	[-(secB), 0]
					// ];

					
					console.log(this.chartData);
					this.generateSpiderChart();
					this.linkExpired = false;
					this.blockUI.stop();
				},
				err => {
					// console.log(err);
					this.linkExpired = true;
					//this.surveyView = 'aerror';
					this.blockUI.stop();
				});
		}

	}

	generateSpiderChart() {
		Highcharts.chart('communication_assessment', {

			chart: {

			},
			tooltip: { enabled: false },
			legend: { enabled: false },
			credits: { enabled: false },
			title: { text: '' },
			xAxis: {
				gridLineWidth: 0,
				min: -100,
				max: 100,
				tickInterval: 10,
				lineColor: 'black',
				offset: -189,
				labels: {
					enabled: false
				}
			},
			yAxis: {
				gridLineWidth: 0,
				min: -100,
				max: 100,
				tickInterval: 10,
				lineWidth: 1,
				lineColor: 'black',
				offset: -258,
				title: {
					text: null
				},
				labels: {
					enabled: false
				}
			},

			series: [{
				// name: 'Current Brand',
				dashStyle: 'solid',
				data: this.chartData
			}]
		});
	}

}

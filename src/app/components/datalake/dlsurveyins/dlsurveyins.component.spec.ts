import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlsurveyinsComponent } from './dlsurveyins.component';

describe('DlsurveyinsComponent', () => {
  let component: DlsurveyinsComponent;
  let fixture: ComponentFixture<DlsurveyinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlsurveyinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlsurveyinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

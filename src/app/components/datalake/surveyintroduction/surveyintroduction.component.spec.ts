import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlresearchsurveyinsComponent } from './dlresearchsurveyins.component';

describe('DlresearchsurveyinsComponent', () => {
  let component: DlresearchsurveyinsComponent;
  let fixture: ComponentFixture<DlresearchsurveyinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlresearchsurveyinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlresearchsurveyinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlregisterComponent } from './dlregister.component';

describe('DlregisterComponent', () => {
  let component: DlregisterComponent;
  let fixture: ComponentFixture<DlregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

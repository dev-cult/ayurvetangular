import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeoplespeakintegrationComponent } from './peoplespeakintegration.component';

describe('PeoplespeakintegrationComponent', () => {
  let component: PeoplespeakintegrationComponent;
  let fixture: ComponentFixture<PeoplespeakintegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeoplespeakintegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeoplespeakintegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
